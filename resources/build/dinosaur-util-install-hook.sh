#!/bin/sh
mkdir -p /usr/lib/finish-install.d
cat <<SCRIPT > /usr/lib/post-base-installer.d/0dinosaur-util-hook.sh
#!/bin/sh
echo 'APT::Periodic::Enable "0";' > /target/etc/apt/apt.conf.d/10dinosaur
echo 'Acquire::Check-Date "false";' >> /target/etc/apt/apt.conf.d/10dinosaur
echo 'DEBIAN_FRONTEND=noninteractive' >> /target/etc/environment
SCRIPT
chmod +x /usr/lib/post-base-installer.d/0dinosaur-util-hook.sh
