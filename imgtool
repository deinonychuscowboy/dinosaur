#!/bin/bash
set -euo pipefail
if [ -n "$(uname -a | grep -ioP "microsoft|cygwin|mingw")" ]; then
	echo "This utility requires sfdisk; run it in a linux-based environment such as the Windows Subsystem for Linux, or a linux virtual machine with a shared host folder."
	exit 1
fi
if [ -z "$(which vbox-img.exe)" ] && [ -z "$(which vbox-img)" ]; then echo "Cannot find vbox-img. Ensure virtualbox is installed and the virtualbox directory is in your PATH environment variable if on windows (and close and re-open your shell)."; exit 1; fi
vboximg="vbox-img"
if [ -z "$(which vbox-img)" ]; then vboximg="vbox-img.exe"; fi
filename=$1
imgname="$(basename $filename | grep -Po '^[^.]*')"
rm -rf out/$imgname
mkdir -p out/$imgname
cd out/$imgname
tar -xvf "../../$filename"
rm -f *.ovf
for source in *.vmdk; do
	diskname="$(echo $source | grep -Po '^[^.]*')"
	vbimgcmd="$vboximg convert --srcfilename $source --dstformat RAW"
	# if gzip is available, compress directly to gz (stream compression) to reduce disk space
	if [ -n "$(which gzip)" ]; then
		# In theory we could do this, but vbox-img seems to be broken
		# $vbimgcmd --stdout | gzip -c > $diskname.raw.gz
		$vbimgcmd --dstfilename $diskname.raw
	else
		$vbimgcmd --dstfilename $diskname.raw
	fi
	rm -f $source
	sfdisk -l -uS $diskname.raw | tee ${diskname}_map.txt
	for partitionline in $(sfdisk -l -uS $diskname.raw | grep -zoP "(?s)Device.*" | tr "\0" "\n" | tail -n +2 | tr "[ \t]" "," | sed "s/,,*/,/g"); do
		date -Iseconds
		number="$(echo $partitionline | cut -d , -f 1 | grep -Po '\d+$')"
		start="$(echo $partitionline | cut -d , -f 2)"
		end="$(echo $partitionline | cut -d , -f 3)"
		blocks="$(echo $partitionline | cut -d , -f 4)"
		ddcommand="dd skip=$start count=$blocks"
		# if gzip is available, compress directly to gz (stream compression) to reduce disk space
		if [ -n "$(which gzip)" ]; then
			# In theory we could do this, but vbox-img seems to be broken
			#cat $diskname.raw.gz | gunzip -c | $ddcommand | gzip -c > ${diskname}_$number.raw.gz
			$ddcommand if=$diskname.raw | gzip -c > ${diskname}_$number.raw.gz
		else
			$ddcommand if=$diskname.raw of=${diskname}_$number.raw
		fi
	done
	rm -f $diskname.raw
	rm -f $diskname.raw.gz
done

echo "To extract compressed images, use gunzip -c [file] > /dev/destination-partition"
