Dinosaur Tooling
================

A bunch of tooling config and scripting that uses Hashi's packer to create a customized ubuntu as an OVA appliance. Useful both to build and rebuild customized ubuntu VMs and to build and rebuild physical machines (via copying the data to a physical disk).

Called Dinosaur thanks to my longstanding tradition of naming my computers after species of dinosaurs.

Prerequisites
-------------
* Supply bin/packer.exe or bin/packer for your platform -- [Packer](https://www.packer.io/downloads.html)
* Supply bin/yaml2json.exe or bin/yaml2json for your platform -- [Yaml2Json](https://github.com/bronze1man/yaml2json/releases)
* Have a valid virtualization platform installed. Dinosaur is designed against VirtualBox, but in theory any platform supported by packer will work with minimal changes.

(Dinosaur will also try the $PATH, if you happen to have those in there.)

Dinosaur's main frontend is a bash script, so if you're on windows, you need some facility to run these. Git Bash, included with git for windows, works well. The [Windows Subsystem for Linux](https://docs.microsoft.com/en-us/windows/wsl/install-win10) works if you're able to use HyperV, but be aware it may conflict with other virtualization platforms like VirtualBox.

Usage
-----

```
cd /path/to/dinosaur/
# just run a basic build
./dinosaur
# specify some variants and the hostname
./dinosaur -i installvariant -c configvariant -n hostname
# ignore cached images for your variants and start over from the install stage
./dinosaur -i installvariant -c configvariant -n hostname install
# ignore cached images for your variants and start over from the build stage (beginning)
./dinosaur -i installvariant -c configvariant -n hostname build
```

See `./dinosaur -h` for more.

Error Handling
--------------

Dinosaur gives you a number of options for how to handle errors in your build process.

* You can make use of the --on-error flag (-e). This will allow any packer operation to be re-executed if it fails. This is a fine-grained level of control, and depending on how your variants and steps are set up, can allow such things as "This script failed, but the VM is still up, try just rerunning the script."
* You can make use of the --autoretry flag (-a). This works on the dinosaur level, and in the event a packer operation aborts and the VM is destroyed, will automatically retry the current step from the beginning with a fresh copy of the VM. Without this flag, you will be asked if you want to retry each time. Using this flag is good if you have a short step which intermittently fails (fairly common with packer) and can allow for unattended build processes.
* You can at any time Ctrl-C to request a clean shutdown following the current step. Ctrl-C again will perform an immediate abort without killing the packer and VM processes, requiring you to clean them up manually (Remove in Virtualbox itself, make sure the folder is gone in the Virtualbox VMs folder, and kill all packer.exe processes in task manager).

Configuration Overview
======================

Stages
------

Dinosaur operates by grouping packer operations into four stages:

- Build
- Install
- Configure
- Package

Each of these stages is a separate packer build based on the previous stage. Stages are cached so that, if you have already built your base linux image from your livecd, installed your software, or set up your users and copied over their standard config files, you don't need to do all that again to create another image based on the same distro with the same options.

The build stage performs the basic task of setting up the VM, inserting a livecd, and running the install program from it. Typically, the used livecd is ubuntu server with a minimal package install set selected (from which you can install ubuntu desktop packages later), but this can be customized. There is a built-in LTS option (-l) to override any custom image selection.

The install stage performs multi-step package installation and removal. It is able to execute custom scripts to e.g. download things from github.

The configure stage sets up users and things like ecryptfs, and other basic operations, like copying any files you want installed per-user. It is able to execute custom scripts to e.g. copy and untar your ~/.config directory.

The package stage finishes up the install, requires users to change their passwords on next login, etc.

Unlike plain packer, Dinosaur uses yaml, because json is awful. Config is otherwise identical to packer's json config; the yaml is translated directly to the json files used by packer. Dinosaur does not support and will not support HCL templates.

Variants
--------

Dinosaur supports different build variants (profiles, configurations) with yaml files in the variants folder. Here, you may create different files that specify variables for the stages to consume. These are just packer var files that are automatically mapped to the correct stages. If you want to use an install variant named xfce that installs the packages for the Xfce desktop environment, you would name the file `install-xfce.yaml`, and select it at runtime with `dinosaur -i xfce`. Some basic examples of variants for the configure and install stages are included. You likely won't often need variants for the build and package stages, but they do support them.

Steps
-----

The install and configure stages can be executed multiple times in a row, each time using a different variant. To do this, either specify the -i or -c flags multiple times on the command line with the names of the variants you want to run in order, or use targets. This allows you to e.g. break up your install packages into multiple separate lists, each of which can be cached individually, so you can have a base list of packages that many images will need, and more specific lists that build on top of that.

Targets
-------

You can predefine lists of variants to run in `targets.ini` in the root directory. INI files are simple and easy to parse: use either `[configure]` or `[install]` to define your section, and then use `target=variant1 variant2` to specify each target you want to define. Then, at runtime, use -I or -C to select the target you want to run.

Build Process
=============

Build Stage
-----------

This stage does the main linux image installation, which is set up by default to use the ubuntu server CD iso. Probably not too much to customize in the yaml for this stage if you're using ubuntu, it's mostly just a shim for the OS install. In the `resources/build` folder, you'll find the autoinstall config files, for customizing the ubuntu install process on modern versions of ubuntu. As of 20.04, ubuntu no longer uses debian preseeding, but the debian preseeding file used for older versions is still included, for customizing the install process on related distros. Additional components required by dinosaur are also installed at this stage, and ubuntu dependencies that are not needed, like cloud-init, are cleaned up (if you really want these, they can be restored in the install step).

You may wish to look at my dinosaur-config repo to see my own customized variants for more detailed examples.

Install Stage
-------------

The install stage works using packagelists, which are simple lists of packages to install or remove. You specify these with the `install_packagelist` key, and what you want done with them (the apt-get command to run) with `install_operation`.

After the packagelists are applied, the install stage executes your custom scripts (from the `resources/install` directory) that are listed in `install_scripts`. Your scripts will start in `/tmp/dinosaur`, and you can wget or curl from `http://$PACKER_HTTP_ADDR/file.txt` to download `file.txt` from the `resources/install` directory to the image you're building.

Remember this stage supports steps, so you can break your packagelists up into multiple variants that will have images cached individually if you don't want them all to be smooshed into one long build that might bomb out at the very end (strongly recommended).

There is an included variant that helps you install the virtualbox guest additions if your built image will run in virtualbox.

You may wish to look at my dinosaur-config repo to see my own customized variants for more detailed examples.

Configure Stage
---------------

This stage is responsible for setting up users and encryption. You can do this by specifying the `admin_users` and `normal_users` variables, which are space-separated lists of usernames. If you would like to modify the default groups these users are added to, you can override `admin_groups` and `normal_groups` (but remember `admin_groups` should contain `sudo` unless you know what you're doing). The `admin_security` and `normal_security` variables may be `ecryptfs` or `none`, and apply to all users in the respective lists. Be aware that as of 20.04 ecryptfs support is starting to get kind of flaky on ubuntu, thanks to systemd keyring changes.

If you have your own configuration files, you can put them in the `resources/configure` directory. You can also create scripts in this directory, and have dinosaur execute these scripts by listing them in the `configure_scripts` variable. Your scripts will start in `/tmp/dinosaur`, and you can wget or curl from `http://$PACKER_HTTP_ADDR/file.txt` to download `file.txt` from the `resources/configure` directory to the image you're building.

Remember this stage supports steps, so you can break your configuration up into multiple variants that will have images cached individually if you don't want them all to be smooshed into one long build that might bomb out at the very end.

You may wish to look at my dinosaur-config repo to see my own customized variants for more detailed examples.

Package Stage
-------------

This stage finishes up a few things and removes the SSH user that packer uses to do the build steps. Not much to see here, other than setting the hostname of the final image -- You can do this with a variant, or with the `--hostname` (`-n`) variable given to dinosaur itself. Also, be aware that by default, dinosaur removes the openssh server, since most systems do not need ssh and it may pose a security risk to leave the server installed in an offline image if a zero-day is later discovered in openssh. If you need openssh on your image, you can specify `retain_ssh_server: "true"` in a packaging variant, but be aware you must log into the built image directly (not via ssh) the first time in order to set passwords.

You may wish to look at my dinosaur-config repo to see my own customized variants for more detailed examples.

Preparing Images and Physical Installs
----------------

After the packaging stage, you will find the final .ova appliance in the `build` folder. This appliance can be imported into any virtualization software. If you intend to use a virtualization service, you can skip the rest of this section and go to "First Startup" below.

If you instead want to install the system onto a physical hard disk, you have a couple more considerations. Firstly, the .ova file needs to be extracted into a disk image format that can be applied to a physical disk. Consider using `imgtool`, included with dinosaur, which will extract the disk and break it into its component raw partition images, compressed to gzip if gzip is available on your system. This tool will require at least as much free space while extracting as Dinosaur used to create your hard disk (default 20GiB) due to a bug in virtualbox, but if you have gzip, the resulting images will be much smaller and suitable for putting on a flash drive afterwards (though if any of your compressed partition images end up over 4GiB, that flash drive cannot be formatted with fat32, as fat32 only supports individual files up to 4GiB in size).

When installing to a physical system, you will need a host OS, such as a liveCD/liveUSB, in order to perform the copy of partitions to the device. It's recommended to use the version of ubuntu you used for the dinosaur build, but if you know grub and any other config you're keeping are already set up properly (e.g. from a previous install), you can use something like TinyCore too. You'll only need `gunzip` and `coreutils` to extract the images, but `parted`, `fdisk`, and/or `gparted` and its assorted filesystem support packages may be helpful to you in this stage, e.g. for partition resizing and filesystem checking.

Installation tips:
- If using the same version of ubuntu to do this install, the easiest way to ensure grub is properly configured is to mount all your partitions in their intended final structure and chroot into your system, then run grub-install and update-grub.
- If using another system like TinyCore to do the final install, ensure your existing hard disk already has a valid grub setup with an identical partition layout to the one you're trying to install.
- Images built in dinosaur in EFI mode are unlikely to be compatible with a system booting with BIOS, and vice versa.
- Make sure the partitions you create on your physical disk are the same size, or larger than, the partitions used in dinosaur. You can easily resize a filesystem up to fill the partition, but creating a filesystem that overruns its partition is a good way to lose data. After copying your partitions, use `e2fsck -f` to be sure the copy proceeded correctly and `resize2fs -pf` to grow a filesystem that is too small.
- Also, be mindful of the difference between GB and GiB (1000-byte vs. 1024-byte prefix systems) and be sure to use them consistently.
- After copying to physical partitions, if GRUB is in PARTUUID mode (the default), if you have either created a new partition table or copied a /boot partition, and cannot chroot and grub-install, you MUST edit your /boot/grub/grub.cfg manually in order to boot the first time, unless you have specified the `GRUB_DISABLE_LINUX_PARTUUID=true` environment variable and run update-grub as part of your build process (recommended). This is because the partitions you created on the physical disk have UUIDs (even msdos ones have pseudo-uuids), and these UUIDs will not match the ones dinosaur knew about when it was creating your image. Unlike filesystem UUIDs, partition UUIDs are not copied over when you dump a filesystem. You can do this by mounting your /boot partition within the install environment and editing the file. Look for `PARTUUID` in grub.cfg and replace its argument with the correct `PARTUUID`s listed in `blkid`. If you did specify to disable partuuids, GRUB will use filesystem uuids, which are copied over, and will be correct as long as you copy the /boot partition.
- If installing over a previous dinosaur-built install, you may be tempted to only copy some partitions, such as skipping your home partition. Be mindful that if you do this, you will need to update /etc/fstab in the partition with your old home partition's UUID. At a minimum, you should probably copy over at least / and /boot, and if you break out things like /var into partitions, you should copy those too.

In case of problems:
- If you don't even get to grub, grub was probably not in the MBR or EFI partition prior to copying partitions (e.g. if you did a windows install for dual-boot prior to this step on a BIOS machine, your MBR would have been wiped out). You should be able to do a grub-install from a liveCD to just fix this without any extra work, but if that doesn't work, do a normal ubuntu install with your intended partition layout, and then re-copy your partitions.
- If you get a grub rescue shell, grub EFI or MBR is not pointing to your /boot partition. The quick way to boot manually in grub 2 is (with an example using the first partition on the first disk): `set root=(hd0,gpt1); set prefix=(hd0,gpt1)/grub; insmod normal; normal`, which should get you back to the "real" grub menu. `(hd0,gpt1)` is an example of the name of your boot partition in grub, but it may be different depending on your drive layout and partition type. You can find the right name with `ls` to list all partitions grub knows about, and then `ls (blahblah)/` on each one to look inside it. Grub can read ext2 partitions but may not be able to read ext4 without a module available, so make sure your boot partition is ext2 for maximum reliability in the event of issues. After booting normally, just run `update-grub` to fix this situation.
- If you get through grub but end up in an `initrd` or `initramfs` environment with busybox, grub is telling linux to look for the wrong root partition. Double check your `PARTUUID` parameters in grub.cfg (may need to go back to the liveCD if there's no text editor in your initramfs). Once you're able to boot normally, running `update-grub` will ensure the file is correct.

First Startup
-------------

Upon first boot, YOU SHOULD NOT LOG IN AS NORMAL. Instead, if you see a GUI login screen like lightdm, press `Ctrl-Alt-F1` to switch to a terminal. Log in as `root` (password `root`). You will be required to change your password when you do so. If you do not want to use the `root` account at all in the future, you may enter a random long password, or disable root login entirely with `passwd -l root` (but please make sure you can log in to another user with sudo permissions first!). All admin accounts created by dinosaur are already set up as `sudo` users and just need to be unlocked for use.

Once logged in as `root`, you will need to change the password (`passwd username`) for each of the accounts you want enabled (the usernames you created in your configure variant, or `ubuntu` if you used the default). This will unlock the accounts and allow you to log in normally as them.

IF YOU ARE NOT USING ECRYPTFS, simply change the password to the final password you want on your accounts. You may then log out of `root` (`exit`), and press `Ctrl-Alt-F7` to switch back to the GUI and log in normally (might need to toggle the username to get the right password prompt to appear in lightdm, it may still ask you to change your password if you don't).

IF YOU ENABLED ECRYPTFS, to avoid issues with systemd, you should instead change the password to the default password that was used in dinosaur's build process. This is identical to the username for each user. You will then need to log into the GUI environment as each user, and change your password through the standard GUI interface for changing passwords in your desktop environment; this will ensure the ecryptfs key is updated properly. After you have done this, open a terminal and run `rm -rf ~/.local/share/keyrings/*` to re-initialize your user keyrings with the new password. Finally, reboot and log in normally to make sure everything is working.

Quirks
------

Dinosaur will handle partition and filesystem details for you to some degree. As long as you re-run the packaging stage for each distinct image you build, your images will have different filesystem and partition UUIDs, so you don't need to worry about filesystem UUID clashes. However, in order to do this, Dinosaur must be able to expect that your root filesystem will be on an ext4 partition, NOT ext3 or another type, as only ext4 (afaik) supports changing the partition metadata without being unmounted. It's also very wise to make sure your /boot folder is on a separate ext2 filesystem (which prevents any possible problems with grub during the renumbering process). Other possible filesystems and partitions are entirely up to you; Dinosaur will attempt to update these as well, though if you're using anything fancy or esoteric I'd appreciate a merge request with the right commands.
